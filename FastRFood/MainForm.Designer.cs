﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using OnBarcode.Barcode.BarcodeScanner;
using System.IO;

using AForge.Video;
using AForge.Video.DirectShow;
using System.Drawing;
using System.Globalization;
using AForge.Imaging.Filters;

namespace FastRFood
{
    partial class MainForm
    {
        
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private bool DeviceExist = false;
        private FilterInfoCollection videoDevices;
        public VideoCaptureDevice videoSource = null;
        private long timeLastSet = 0;
        private long timeLastChecked = 0;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtOrder = new System.Windows.Forms.RichTextBox();
            this.picScan = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picScan)).BeginInit();
            this.SuspendLayout();
            // 
            // txtOrder
            // 
            this.txtOrder.BackColor = System.Drawing.Color.Blue;
            this.txtOrder.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtOrder.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.txtOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOrder.ForeColor = System.Drawing.Color.Black;
            this.txtOrder.Location = new System.Drawing.Point(800, 12);
            this.txtOrder.Margin = new System.Windows.Forms.Padding(4);
            this.txtOrder.Name = "txtOrder";
            this.txtOrder.ReadOnly = true;
            this.txtOrder.Size = new System.Drawing.Size((Screen.FromControl(this).Bounds.Width) / 2, Screen.FromControl(this).Bounds.Height);
            this.txtOrder.TabIndex = 0;
            this.txtOrder.Text = "<- Scan your barcode";
            this.txtOrder.TextChanged += new System.EventHandler(this.txtOrder_TextChanged);
            // 
            // picScan
            // 
            this.picScan.BackColor = System.Drawing.Color.LightSlateGray;
            this.picScan.Location = new System.Drawing.Point(10, 10);
            this.picScan.Name = "picScan";
            this.picScan.Size = new System.Drawing.Size(768, 432);
            this.picScan.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picScan.TabIndex = 1;
            this.picScan.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Blue;
            this.ClientSize = new System.Drawing.Size(1661, 609);
            this.Controls.Add(this.picScan);
            this.Controls.Add(this.txtOrder);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.picScan)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        /*private void MainForm_Load(object sender, System.EventArgs e)
        {
            begin();
        }*/
        

        //get total received frame at 1 second tick
       

        //prevent sudden close while device is running
        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            CloseVideoSource();
        }

        #endregion

        private System.Windows.Forms.RichTextBox txtOrder;

        private void begin()
        {
            Console.Write("Begin was called");
            
            

            videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            videoSource = new VideoCaptureDevice(videoDevices[0].MonikerString);
            //videoSource.DesiredFrameSize = new Size(160, 160);
            videoSource.NewFrame += new NewFrameEventHandler(video_NewFrame);
            
            //CloseVideoSource();
            //videoSource.DesiredFrameSize = new Size(160, 120);
            //videoSource.DesiredFrameRate = 10;
            CloseVideoSource();
            videoSource.Start();
        }

        void AppendText(RichTextBox box, Color color, string text)
        {
            int start = box.TextLength;
            box.AppendText(text);
            int end = box.TextLength;

            // Textbox may transform chars, so (end-start) != text.Length
            box.Select(start, end - start);
            {
                box.SelectionColor = color;
                // could set box.SelectionBackColor, box.SelectionFont too.
            }
            box.SelectionLength = 0; // clear
        }

        public void AddText(RichTextBox box, Color color, string text, HorizontalAlignment align)
        {
            int start = box.TextLength;
            box.AppendText(text);
            int end = box.TextLength;

            // Textbox may transform chars, so (end-start) != text.Length
            box.Select(start, end - start);
            {
                box.SelectionColor = color;
                box.SelectionAlignment = align;
                // could set box.SelectionBackColor, box.SelectionFont too.
            }
            box.SelectionLength = 0; // clear
        }
        private void setOrder(String order)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(setOrder), new object[] { order });
                return;
            }

            String[] items = order.Split('|');
            decimal total = 0;
            txtOrder.Text = "";
            txtOrder.ForeColor = Color.Black;
            AddText(txtOrder, Color.Black, "Your order:\n\n",HorizontalAlignment.Left);
            foreach(String item in items)
            {
                //String name = item.Split('#')[0];
                //txtOrder.AppendText(item.Split('#')[0]);
                AddText(txtOrder,Color.Black,item.Split('#')[0]+"\n",HorizontalAlignment.Left);
                var numberFormatInfo = new NumberFormatInfo();
                numberFormatInfo.NumberDecimalSeparator = ".";
                decimal price=decimal.Parse(item.Split('#')[1],numberFormatInfo);
                AddText(txtOrder, Color.DarkGreen, "\t\tR"+price+"\n",HorizontalAlignment.Right);
                total += price;
            }
            AddText(txtOrder, Color.DarkGreen, "Total: \n",HorizontalAlignment.Left);
            AddText(txtOrder, Color.Red, "R"+total,HorizontalAlignment.Right);
        }

        private void video_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            Bitmap img = (Bitmap)eventArgs.Frame.Clone();
            //ResizeNearestNeighbor filter = new ResizeNearestNeighbor(768, 432);
            // apply the filter
            //img = filter.Apply(img);
            if ((DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond) - timeLastSet > 5000)
            {
                if ((DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond) - timeLastChecked > 300)
                {
                    string[] barcodes = BarcodeScanner.ScanSingleBarcode(img, BarcodeType.QRCode);
                    timeLastChecked = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
                    if (barcodes != null)
                    {
                        if(Char.IsNumber(barcodes[0][0]))
                        {
                            timeLastSet = 0;
                            timeLastChecked = 0;
                            return;
                        }
                        timeLastSet = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
                        setOrder(barcodes[0]);
                    }
                }
            }
            
            picScan.Image = img;
        }
        //close the device safely
        private void CloseVideoSource()
        {
            if (!(videoSource == null))
                if (videoSource.IsRunning)
                {
                    videoSource.SignalToStop();
                    videoSource = null;
                }
        }

        private PictureBox picScan;
        
    }


}

